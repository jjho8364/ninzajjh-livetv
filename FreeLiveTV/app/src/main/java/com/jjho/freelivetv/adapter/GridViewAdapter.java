package com.jjho.freelivetv.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.jjho.freelivetv.R;
import com.jjho.freelivetv.model.GridViewItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-08.
 */
public class GridViewAdapter extends BaseAdapter {


    Activity context;
    ArrayList<GridViewItem> gridArray;
    LayoutInflater inf;
    int layout;

    static class ViewHolder {
        public ImageView imageView;
        public String urlPath;
    }


    public GridViewAdapter(Activity context, ArrayList<GridViewItem> gridArray, int layout) {
        this.context = context;
        this.gridArray = gridArray;
        this.layout = layout;
        inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (convertView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inf.inflate(layout, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageView= (ImageView)rowView.findViewById(R.id.fr1_gridview_img);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();
        GridViewItem data = gridArray.get(position);


        //convertView = inf.inflate(layout, null);
        //ImageView iv = (ImageView)convertView.findViewById(R.id.imageView1);
        //iv.setImageResource(img[position]);

        //ViewHolder viewHolder = new ViewHolder();

        //viewHolder.imageView= (ImageView)convertView.findViewById(R.id.fr1_gridview_img);
        //convertView.setTag(viewHolder);

        //String url = "";

        //Fr1GridViewItem data = gridArray.get(position);

        //holder.imageView.setImageResource(data.getLayout());
        Picasso.with(context).load(data.getImgUrl()).into(holder.imageView);
        holder.urlPath = data.getUrl();

        return rowView;

    }






    @Override
    public int getCount() {
        return gridArray.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
