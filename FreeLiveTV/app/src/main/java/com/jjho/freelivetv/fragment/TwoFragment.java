package com.jjho.freelivetv.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.jjho.freelivetv.R;
import com.jjho.freelivetv.activity.VideoViewActivity;
import com.jjho.freelivetv.adapter.GridViewAdapter;
import com.jjho.freelivetv.model.GridViewItem;
import com.jjho.freelivetv.utils.NetworkUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016-07-08.
 */
public class TwoFragment extends Fragment implements View.OnClickListener {
    private final String TAG = " OneFragment - ";
    private ImageView sbs_ori;
    private GridView gridView;
    private GridViewAdapter adapter;

    private Button btn;

    String packageName = "";

    public TwoFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;

        // mx player 설치확인
        PackageManager pm = getActivity().getPackageManager();
        List<ApplicationInfo> list = pm.getInstalledApplications(0);
        for (ApplicationInfo applicationInfo : list) {
            String pName = applicationInfo.packageName;   // 앱 패키지
            if(pName.equals("com.mxtech.videoplayer.pro")){
                packageName = "com.mxtech.videoplayer.pro";
                Log.d(TAG, "pacakge : pro ");
                break;
            } else if(pName.equals("com.mxtech.videoplayer.ad")){
                packageName = "com.mxtech.videoplayer.ad";
                Log.d(TAG, "pacakge : ad ");
                break;
            }
        }

        String model = Build.MODEL.toLowerCase();
        TelephonyManager telephony = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String operator = telephony.getNetworkOperator();
        int portrait_width_pixel=Math.min(this.getResources().getDisplayMetrics().widthPixels, this.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch=this.getResources().getDisplayMetrics().densityDpi;
        boolean isPhone = true;
        boolean contactsFlag = false;
        boolean phoneNumFlag = false;
        boolean operatorFlag = false;
        float virutal_width_inch=portrait_width_pixel/dots_per_virtual_inch;
        if (virutal_width_inch <= 2) { isPhone = true; } else { isPhone = false; }
        //Cursor c = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        //int contactsCnt = c.getCount();
        //if(contactsCnt < 20){ contactsFlag = true; }
        TelephonyManager telManager = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        //String phoneNum = telManager.getLine1Number();
        //if(phoneNum == null || phoneNum.equals("")){ phoneNumFlag = true; }
        if(operator == null || operator.equals("")){ operatorFlag = true; }



        if ( (model.equals("sph-d720") || model.contains("nexus") || operatorFlag) && isPhone) {
            view = inflater.inflate(R.layout.depend, container, false);
        } else if(packageName.equals("")){
            view = inflater.inflate(R.layout.installmxplayer, container, false);

            btn = (Button)view.findViewById(R.id.btn_mxplayer);
            btn.setOnClickListener(this);

        } else {
            view = inflater.inflate(R.layout.fragment01, container, false);

            if(NetworkUtil.getConnectivity(getActivity())){
                final ArrayList<GridViewItem> gridArr = new ArrayList<GridViewItem>();

                // kbs2
                gridArr.add(new GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/kbs2-banner.jpg", "https://p5.skypark.tk/stream.m3u8?type=so&id=k02,h,2&auth=a2af85c8107966425ca01eea6ecb5b1657cc50c4"));
                // mbc
                gridArr.add(new GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/mbc-banner.jpg", "https://p3.skypark.tk/stream.m3u8?type=so&id=m01,h,1&auth=23d6537716920251d09d725d022d9c4557cc5104"));
                // sbs
                gridArr.add(new GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/sbs-banner.jpg", "https://p2.skypark.tk/stream.m3u8?type=so&id=s01,h,2&auth=2d83e6dec73d53e919b339cbae0f5bb457cc5154"));
                // jtbc
                gridArr.add(new GridViewItem("http://www.sinktv.com/wp-content/uploads/2014/03/jtbc-banner.jpg", "http://jcubelive.ktics.co.kr/jtbcmobile5k/_definst_/jtbc_500k_mobile.stream/playlist.m3u8?token=fa7082ce90cbb5c4257d8b8a0375b21d&expr=57cc4956&file=jtbc_500k_mobile.stream"));
                // mnet
                gridArr.add(new GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/mnet-banner.jpg", "https://p4.skypark.tk/stream.m3u8?type=st&id=1,sd&auth=12cd2f1b87bbdd89559d0b8842b4f96d57cc3c5c"));
                // tvn
                gridArr.add(new GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/tvn-banner.jpg", "https://p3.skypark.tk/stream.m3u8?type=st&id=3,sd&auth=e87ec1f482628b1ecf2a09113c20a83357cc3cdc&nored"));
                // onstyle
                gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=c53x53&quality=10&q=http://post.phinf.naver.net/20160627_235/itsonstyle_1467017635816GHgRt_JPEG/itsonstyle_7682564314683993317.jpg?type=f120_120", "https://p.skypark.tk/stream.m3u8?type=st&id=6,sd&auth=a04234ee4f0f47632f8f3d10529222cd57cc3d59"));
                // oliveTV
                gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=c53x53&quality=10&q=http://post.phinf.naver.net/20160329_141/olive_post_1459231450194AioJG_JPEG/olive_post_6373326454529821086.jpg?type=f120_120", "https://p2.skypark.tk/stream.m3u8?type=st&id=5,sd&auth=cf044dedd8993b956341896c8ba987b357cc3db4"));
                // 온게임넷
                gridArr.add(new GridViewItem("http://img.lifestyler.co.kr/uploads/program/1/1102/skin/f130821426772198575(0).png", "https://p2.skypark.tk/stream.m3u8?type=st&id=7,hd&auth=833ef44755e854e3a5edf9df719e147a57cc3e12"));
                // xtm
                gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=c53x53&quality=10&q=http://post.phinf.naver.net/20150831_112/xtm_tv_1441014914882Nd5KD_PNG/xtm_tv_5403547753951263790.png?type=f120_120", "https://p.skypark.tk/stream.m3u8?type=st&id=8,sd&auth=5e21157825d8ea4cc76e6d9717899eee57cc3e9e"));
                // kbs drama
                gridArr.add(new GridViewItem("http://www.kbsn.co.kr/images/common/logo_drama.png", "http://hls.live.kbskme.gscdn.com/kbsn_drama/_definst_/kbsn_drama_5.stream/chunklist.m3u8?wowzasessionid=1769197942&csu=false&tid=d4379e743af970184902b8851abe9089"));
                // kbs joy
                gridArr.add(new GridViewItem("http://www.kbsn.co.kr/images/common/logo_joy.png", "http://hls.live.kbskme.gscdn.com/kbsn_joy/_definst_/kbsn_joy_5.stream/chunklist.m3u8?wowzasessionid=1026891737&csu=false&tid=fc7eefcaef3741edd163a74e66cf0bf6"));
                // kbs n sports
                gridArr.add(new GridViewItem("http://www.kbsn.co.kr/images/common/logo_sports.png", "http://hls.live.kbskme.gscdn.com/kbsn_sports/_definst_/kbsn_sports_5.stream/chunklist.m3u8?wowzasessionid=740474453&csu=false&tid=6400a716b487eba54ff6f3e9d89c810d"));
                // kbs news 24
                gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/79/16/57_667916_poster_image_1417502523733.jpg", "http://hls.live.kbskme.gscdn.com/news24/_definst_/news24_800.stream/chunklist.m3u8?wowzasessionid=1739550510&csu=false&tid=90a4f58498340643df3356121254911a"));
                // mbc 드라마넷
                gridArr.add(new GridViewItem("https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-1/c19.0.50.50/p50x50/13319839_787215218044729_115602438649387075_n.png?oh=2f90fd3c377691cfc569e7a59351d378&oe=5837CC1E", "http://m.onairtv.mbcplus.com/m_plus_drama/_definst_/drama.stream/chunklist_w371088568.m3u8?csu=false&tid=f1e758d8b7f30bee80a409c8aaa96c5d"));
                // mbc everyone
                gridArr.add(new GridViewItem("https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-1/p50x50/10270433_657663084304839_2551115337047960258_n.jpg?oh=4b62c33a32d77808d63025ce4037f2c0&oe=587E6028", "http://m.onairtv.mbcplus.com/m_plus_everyone/_definst_/everyone.stream/chunklist_w1576535129.m3u8?csu=false&tid=33d81d46f38a57b42fe53be7ae722208"));
                // mbc music
                gridArr.add(new GridViewItem("https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-1/p50x50/10516699_985711421453997_6547383953568675538_n.jpg?oh=6ed9ceaa5d7ffd1c7473ca5061eb00a4&oe=5843FC7C", "http://m.onairtv.mbcplus.com/m_plus_music/_definst_/music.stream/chunklist_w735035624.m3u8?csu=false&tid=33d81d46f38a57b49edbcf35a8c02ee2"));
                // sbs plus
                gridArr.add(new GridViewItem("http://img.sbs.co.kr/sw11/gnb/common/logo_plus.jpg", "http://live.sbs.co.kr/sbspluspc/_definst_/sbsplus3.stream/playlist.m3u8?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzMwMDY5MjEsInBhdGgiOiIvc2JzcGx1c3BjL19kZWZpbnN0Xy9zYnNwbHVzMy5zdHJlYW0iLCJkdXJhdGlvbiI6LTF9.8FW0wNYHGNDuTvks-7LEUHfnvVrCi9voe9KOibsilzY"));
                // sbs fune
                gridArr.add(new GridViewItem("http://img.sbs.co.kr/sw11/gnb/common/logo_fune.jpg", "http://live.sbs.co.kr/sbsetvpc/_definst_/sbsetv3.stream/playlist.m3u8?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzMwMDcwMzMsInBhdGgiOiIvc2JzZXR2cGMvX2RlZmluc3RfL3Nic2V0djMuc3RyZWFtIiwiZHVyYXRpb24iOi0xfQ.w6KFsNjQV5BiW0LbHvDr6bKkq27l67D_BHGXp3hhdlk"));
                // sbs golf
                gridArr.add(new GridViewItem("http://img.sbs.co.kr/sw11/gnb/common/logo_golf.jpg", "http://live.sbs.co.kr/sbsgolf/_definst_/sbsgolf3.stream/playlist.m3u8?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzMwMDcwODIsInBhdGgiOiIvc2JzZ29sZi9fZGVmaW5zdF8vc2JzZ29sZjMuc3RyZWFtIiwiZHVyYXRpb24iOi0xfQ.z5Q6CQ-1mCxR5Ya1a2tPmhIipzlnqdp-SZAAFZts8hI"));
                // sbs sports
                gridArr.add(new GridViewItem("http://img.sbs.co.kr/sw11/gnb/common/logo_sports.jpg", "http://live.sbs.co.kr/sbsespnpc/_definst_/sbsespn3.stream/playlist.m3u8?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzMwMDcxNTcsInBhdGgiOiIvc2JzZXNwbnBjL19kZWZpbnN0Xy9zYnNlc3BuMy5zdHJlYW0iLCJkdXJhdGlvbiI6LTF9.ihbq_Ve4vZFIiPcaNcv9ufjSi4RnnKLuTrF-oihWDZk"));
                // sbs mtv
                gridArr.add(new GridViewItem("http://www.mtv.co.kr/gsp/sbs-mtv-footer-logo.jpg", "http://live.sbs.co.kr/sbsmtvpc/_definst_/sbsmtv1.stream/playlist.m3u8?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzMwMDcyMDQsInBhdGgiOiIvc2JzbXR2cGMvX2RlZmluc3RfL3Nic210djEuc3RyZWFtIiwiZHVyYXRpb24iOi0xfQ.PcpgqdQxkZeGq3J5AyjAO6c9PpRA4jDrWVp2dAe8tGk"));
                // channel a
                gridArr.add(new GridViewItem("http://www.sinktv.com/wp-content/uploads/2014/03/channela-banner.jpg", "http://channelalive.ktics.co.kr/pclive/_definst_/ch1.stream/playlist.m3u8?token=eafd5494031ae8cbdbde660322364fdc&expr=57cc4ee3"));
                // channel a plus
                gridArr.add(new GridViewItem("http://www.channelaplus.com/images/main/gnb_left_tit.jpg", "http://edge1.everyon.tv/etv1sb/phd1076/playlist.m3u8"));

                //gridArr.add(new GridViewItem("", ""));
                //gridArr.add(new GridViewItem("", ""));
                //gridArr.add(new GridViewItem("", ""));
                //gridArr.add(new GridViewItem("", ""));
                //gridArr.add(new GridViewItem("", ""));
                //gridArr.add(new GridViewItem("", ""));


                //gridArr.add(new GridViewItem("http://kbsworld.kbs.co.kr/images/main_logo.jpg", ""));


                gridView = (GridView)view.findViewById(R.id.fr1_gridview);
                adapter = new GridViewAdapter(getActivity(), gridArr, R.layout.gridviewitem);
                gridView.setAdapter(adapter);

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Log.d(" clicked - ", " gridview image ");

                        if(NetworkUtil.getConnectivity(getActivity())){
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            Uri videoUri = Uri.parse(gridArr.get(position).getUrl());
                            intent.setDataAndType( videoUri, "application/x-mpegURL" );
                            intent.putExtra("decode_mode", (byte)1);
                            intent.putExtra("video_zoom", 0);
                            intent.setPackage(packageName);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getActivity(), "check your network connection status", Toast.LENGTH_SHORT).show();
                        }


                    }
                });
            } else {
                Toast.makeText(getActivity(), "check your network connection status", Toast.LENGTH_SHORT).show();
            }

        }

        return view;
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.btn_mxplayer :
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.mxtech.videoplayer.ad"));
                startActivity(intent);
                break;

        }
    }
}
