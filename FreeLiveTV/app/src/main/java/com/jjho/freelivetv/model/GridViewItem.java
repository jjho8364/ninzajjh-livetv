package com.jjho.freelivetv.model;

/**
 * Created by Administrator on 2016-07-08.
 */
public class GridViewItem {
    //int layout;
    String imgUrl;
    String url;

    public GridViewItem(){

    }

    public GridViewItem(String imgUrl, String url) {
        this.imgUrl = imgUrl;
        this.url = url;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /*public Fr1GridViewItem(int layout, String url) {
        this.layout = layout;
        this.url = url;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }*/

}
