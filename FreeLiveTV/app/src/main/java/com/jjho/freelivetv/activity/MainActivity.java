package com.jjho.freelivetv.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.jjho.freelivetv.R;
import com.jjho.freelivetv.fragment.OneFragment;
import com.jjho.freelivetv.fragment.TwoFragment;

public class MainActivity extends FragmentActivity implements View.OnClickListener {

    private final String TAG = " MainActivity - ";
    private int mCurrentFragmentIndex;
    public final static int FRAGMENT_ONE = 0;
    public final static int FRAGMENT_TWO = 1;
    public final static int FRAGMENT_THREE = 2;
    public final static int FRAGMENT_FORTH = 3;

    private TextView fragment01;
    private TextView fragment02;
    private TextView fragment03;
    private TextView fragment04;

    private InterstitialAd interstitialAd;
    private InterstitialAd interstitialAd2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        AdView mAdViewUpper = (AdView) findViewById(R.id.adView_upper);
        //AdView mAdViewLower = (AdView) findViewById(R.id.adView_lower);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdViewUpper.loadAd(adRequest);
        //mAdViewLower.loadAd(adRequest);

        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-9440374750128282/7801138050");
        interstitialAd.loadAd(adRequest);

        interstitialAd2 = new InterstitialAd(this);
        interstitialAd2.setAdUnitId("ca-app-pub-9440374750128282/6091694850");
        interstitialAd2.loadAd(adRequest);

        interstitialAd2.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                interstitialAd2.show();
            }
        });

        fragment01 = (TextView)findViewById(R.id.tv_fragment01);
        //fragment02 = (TextView)findViewById(R.id.tv_fragment02);
        //fragment03 = (TextView)findViewById(R.id.tv_fragment03);


        fragment01.setOnClickListener(this);
        //fragment02.setOnClickListener(this);
        //fragment03.setOnClickListener(this);

        mCurrentFragmentIndex = FRAGMENT_ONE;     // 첫 Fragment 를 초기화
        fragmentReplace(mCurrentFragmentIndex);


    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.tv_fragment01 :
                //offColorTv();
                //fragment01.setBackgroundResource(R.color.purple_300);

                mCurrentFragmentIndex = FRAGMENT_ONE;
                fragmentReplace(mCurrentFragmentIndex);
                break;

            /*case R.id.tv_fragment02 :
                offColorTv();
                fragment02.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_TWO;
                fragmentReplace(mCurrentFragmentIndex);
                break;*/

            /*case R.id.tv_fragment03 :
                //mCurrentFragmentIndex = FRAGMENT_THREE;
                //fragmentReplace(mCurrentFragmentIndex);
                break;*/

        }
    }

    public void fragmentReplace(int reqNewFragmentIndex) {

        Fragment newFragment = null;
        Log.d(TAG, "fragmentReplace " + reqNewFragmentIndex);
        newFragment = getFragment(reqNewFragmentIndex);
        // replace fragment
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.ll_fragment, newFragment);
        // Commit the transaction
        transaction.commit();

    }

    private Fragment getFragment(int idx) {
        Fragment newFragment = null;

        switch (idx) {
            case FRAGMENT_ONE:
                newFragment = new OneFragment();
                break;
            /*case FRAGMENT_TWO:
                newFragment = new TwoFragment();
                break;*/
            /*case FRAGMENT_THREE:
                newFragment = new ThreeFragment();
                break;*/

            default:
                Log.d(TAG, "Unhandle case");
                break;
        }

        return newFragment;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(interstitialAd.isLoaded()){
            interstitialAd.show();
        }
    }

    public void offColorTv(){
        fragment01.setBackgroundResource(R.color.lightblue_300);
        //fragment02.setBackgroundResource(R.color.lightblue_300);
        //fragment03.setBackgroundResource(R.color.lightblue_300);
    }


}
